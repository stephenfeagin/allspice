---
# site.yaml

- name: Configure application
  hosts: all
  become: true
  become_method: sudo

  vars:
    repository_url: https://github.com/stephenfeagin/allspice.git
    repository_path: /home/vagrant/allspice
    ansible_user: vagrant

  environment:
    PIPENV_VENV_IN_PROJECT: 1
    DJANGO_SETTINGS_MODULE: allspice.settings.dev

  tasks:
    - name: Install packages
      apt:
        name: "{{ packages }}"
        state: present
        update_cache: yes
      vars:
        packages:
          - git
          - python3
          - python3-pip
          - nginx

    - name: Install pipenv
      pip:
        name: pipenv
        executable: /usr/bin/pip3
        state: present

    - name: Clone repository
      git:
        repo: "{{ repository_url  }}"
        dest: "{{ repository_path }}"
        force: yes

    - name: Ensure user has permissions on repo path
      file:
        path: "{{ repository_path }}"
        state: directory
        mode: "u+rwx"
        owner: "{{ ansible_user }}"

    - name: Install requirements
      command: pipenv install
      args:
        chdir: "{{ repository_path }}"

    - name: Allow long hostnames in nginx
      lineinfile:
        dest: /etc/nginx/nginx.conf
        regexp: '(\s+)#? ?server_names_hash_bucket_size'
        backrefs: yes
        line: '\1server_names_hash_bucket_size 64;'

    - name: Add Nginx config to sites-available
      template:
        src: ./nginx.conf.j2
        dest: /etc/nginx/sites-available/vagrant
      notify:
        - restart nginx

    - name: Add symlink in Nginx sites-enabled
      file:
        src: /etc/nginx/sites-available/vagrant
        dest: /etc/nginx/sites-enabled/vagrant
        state: link
      notify:
        - restart nginx

    - name: Delete default conf from Nginx sites-enabled
      file:
        path: /etc/nginx/sites-enabled/default
        state: absent
      notify:
        - restart nginx

    - name: Write gunicorn service script
      template:
        src: ./gunicorn.service.j2
        dest: /etc/systemd/system/gunicorn-vagrant.service
      notify:
        - restart gunicorn

  handlers:
    - name: restart nginx
      service:
        name: nginx
        state: restarted

    - name: restart gunicorn
      systemd:
        name: gunicorn-vagrant
        daemon_reload: yes
        enabled: yes
        state: restarted
