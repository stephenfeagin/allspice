# Allspice

## A recipe app for Viteagins

This project is licensed under the terms of the MIT license.

## Dependencies

### Application  
- Python &geq; 3.6
- Django &geq; 2.0
- Gunicorn &geq; 19.9

### System
- Ansible &geq; 2.6  
- Pipenv &geq; 2018.7.1
