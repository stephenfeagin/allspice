from django.conf.urls import url

from recipes.views import home_page


urlpatterns = [
    url('', home_page, name='home')
]
